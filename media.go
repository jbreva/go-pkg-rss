package feeder

type Media struct {
	Url    string
	Width  int
	Height int
	Type   string
}
